package com.practice.test;

import java.util.ArrayList;
import java.util.Iterator;

public class CollectionTest {

	public static void main(String[] args) {
		System.out.println("Main Thread" + Thread.currentThread().isAlive());
		
		Mythread firstThread = new Mythread();
		firstThread.setName("FirstThread");
		
		Mythread secondThread = new Mythread();
		secondThread.setName("Second Thread");
		
		firstThread.start();
		secondThread.start();
		System.out.println("Main Thread" + Thread.currentThread().isAlive());
	}
}
