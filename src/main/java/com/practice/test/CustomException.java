package com.practice.test;

public class CustomException extends RuntimeException{

	CustomException(String message){
		super(message);
	}
}
