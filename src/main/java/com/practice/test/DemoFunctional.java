package com.practice.test;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class DemoFunctional {
	public static void main(String[] args) {
		//FunctionalInterfaceInterface interface1=new FunctionalImplementation();
		//interface1.display();
		
		/*FunctionalInterfaceInterface interface1=new FunctionalInterfaceInterface() {
			public void display() {
				System.out.println("Inside demo class");
			}
		};
		interface1.display();*/
		
		
		FunctionalInterfaceInterface interface1=()->System.out.println("Inside lambda expression");interface1.display();
		Consumer<Integer> consumer=i->{
			int output=i*2;
			System.out.println(output);
			};
		consumer.accept(100);
		
		Supplier<Integer> supplier=()->new Random().nextInt(1000);
		System.out.println(supplier.get());
		
		
		Predicate<Integer> predicate=i->i>100;
		System.out.println("Predicate test:::"+predicate.test(200));
		
		
		
		
		
	}
}
