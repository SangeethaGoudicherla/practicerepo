package com.practice.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class Book {
	private final String bookName;
	private final List<String> topics;

	public Book(String bookName, List<String> topics) {
		this.bookName = bookName;
		List<String> temptopics = new ArrayList<>();
		Iterator<String> iterator = topics.iterator();
		while (iterator.hasNext()) {
			temptopics.add(iterator.next());
		}
		this.topics = temptopics;
	}

	public String getBookName() {
		return bookName;
	}

	public List<String> getTopics() {
		Iterator<String> iterator = this.topics.iterator();
		while (iterator.hasNext()) {
			topics.add(iterator.next());
		}
		return topics;
	}

}

class ImmutableExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*List<String> topics=new ArrayList<>();
		topics.add("JVM");
		topics.add("JRE");
Book book=new Book("Java basics", topics);
System.out.println(book.getBookName());
System.out.println(book.getTopics());*/
		try {
			Runtime.getRuntime().exec("Documents");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
