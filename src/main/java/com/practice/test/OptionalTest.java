package com.practice.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OptionalTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//Returns an empty Optional.
		// TODO Auto-generated method stub
		Optional<String> string = Optional.empty();
		System.out.println(string.isEmpty());
		System.out.println(string.isPresent());

		
		//Returns an Optional which contains a non-null value.
		String str = "Test";
		Optional<String> str1 = Optional.of(str);
		System.out.println(str1.get());
		System.out.println(str1.isEmpty());
		System.out.println(str1.isPresent());
		
		//Will return an Optional with a specific value or an empty Optional if the parameter is null.
		String str2 = "";
		Optional<String> str2Opt = Optional.ofNullable(str2);
		System.out.println(str2Opt.get());
		
		
		
		//get a List<String> and in the case of null you want to substitute it with a new instance of an ArrayList<String>
		List<String> list=new ArrayList<>();
		list.add("Streams");
		list.add("Lambda expresssion");
		
		Optional<List<String>> listoptional=Optional.of(list);
		
		List<String> newList=listoptional.orElseGet(()->new ArrayList<>());
		System.out.println("Size::"+newList);
		
		
	}

}
