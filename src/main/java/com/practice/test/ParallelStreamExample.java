package com.practice.test;

import java.util.Arrays;
import java.util.List;

public class ParallelStreamExample {
	
	public static void main(String[] args) {
		
		List<String> list=Arrays.asList("H","E","L","L","O");
		System.out.println("Stream");
		
		list.stream().forEach(i->System.out.println(i));
		System.out.println("parallel Stream");
		
		list.parallelStream().forEach(i->System.out.println(i));
		//Ordered way
		System.out.println("parallel Stream in ordered");
		list.parallelStream().forEachOrdered(i->System.out.println(i));
		
	}
}
