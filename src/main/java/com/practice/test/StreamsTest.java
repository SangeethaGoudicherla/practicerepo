package com.practice.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamsTest {

	public static void main(String[] args) {

		List<Student> studentList = new ArrayList<>();

		Student student1 = new Student();
		student1.setStudentId(1l);
		student1.setStudentName("Dharma");
		student1.setAge(10l);

		Student student2 = new Student();
		student2.setStudentId(2l);
		student2.setStudentName("Soujanya");
		student2.setAge(12l);

		studentList.add(student1);
		studentList.add(student2);

		// List to Map
		System.out.println("List to map");
		Map<Long, String> studentMap = studentList.stream()
				.collect(Collectors.toMap(Student::getStudentId, Student::getStudentName));
		studentMap.forEach((key, value) -> {
			System.out.println(key);
			System.out.println(value);
		});

		// Map values into List
		List<String> studentNames = new ArrayList<>(studentMap.values());
		/*
		 * studentMap.keySet().forEach(key -> { String value = studentMap.get(key);
		 * studentNames.add(value); });
		 */
		System.out.println("Map values into list");
		studentNames.forEach(name -> System.out.println(name));

		int arr[] = { 2, 1, 4, 3, 5 };
		IntStream mapStream = Arrays.stream(arr);
		System.out.println("--Map--");
		mapStream.map(i -> i + 1).forEach(i -> System.out.println(i));

		System.out.println("Converting student names to uppercase using map");
		studentNames.stream().map(name -> name.toUpperCase()).forEach(n -> System.out.println(n));

		System.out.println("Student age greater than 10");

		studentList.stream().filter(student -> student.getAge() > 10).collect(Collectors.toList())
				.forEach(n -> System.out.println(n.getStudentName()));

//Adding courses to student

		List<Course> coursesList1 = new ArrayList<>();

		List<Course> coursesList2 = new ArrayList<>();

		Course course1 = new Course();
		course1.setCourseId(1l);
		course1.setCourseName("Java");
		coursesList1.add(course1);

		Course course2 = new Course();
		course2.setCourseId(2l);
		course2.setCourseName("SQL");

		coursesList1.add(course2);

		Course course3 = new Course();
		course3.setCourseId(3l);
		course3.setCourseName("SpringBoot and microservices");
		coursesList2.add(course3);
		coursesList2.add(course2);

		student1.setCourses(coursesList1);
		student2.setCourses(coursesList2);

		System.out.println("Student with SQL");
		studentList.stream().filter(student -> student.getCourses().contains(course2)).collect(Collectors.toList())
				.forEach(s -> System.out.println(s.getStudentName()));

		// 9 -11 ==10

		Predicate<Student> ageGreater = student -> student.getAge() > 10;
		Predicate<Student> ageLessThan = student -> student.getAge() < 20;

		studentList.stream().filter(ageGreater.and(ageLessThan))
				.forEach(s -> System.out.println("Age greater than 9 and less than 11" + s.getStudentName()));

		// Creating a list of Prime Numbers
		List<Integer> PrimeNumbers = Arrays.asList(5, 7, 11, 13);

		List<Integer> OddNumbers = Arrays.asList(1, 3, 5);

		// Creating a list of Even Numbers
		List<Integer> EvenNumbers = Arrays.asList(2, 4, 6, 8);

		List<List<Integer>> listOfListofInts = Arrays.asList(PrimeNumbers, OddNumbers, EvenNumbers);

		System.out.println("The Structure before flattening is : " + listOfListofInts);

		List<Integer> listofInts = listOfListofInts.stream().flatMap(l -> l.stream()).collect(Collectors.toList());

		System.out.println("The Structure after flattening is : " + listofInts);

		HashMap<Long, String> testMap1 = new HashMap<>();
		testMap1.put(1l, "ABC");
		testMap1.put(2l, "XYZ");

		HashMap<Long, String> testMap2 = new HashMap<>();
		testMap2.put(3l, "ZZZ");
		testMap2.put(4l, "OOO");

		HashMap<String, HashMap<Long, String>> mainMap = new HashMap<>();

		mainMap.put("key1", testMap1);
		mainMap.put("key2", testMap2);

		System.out.println("FlatMap with HashMap");
		mainMap.entrySet().forEach(a -> System.out.println(a));

		System.out.println("FlatMap with HashMap after flattening");
		mainMap.entrySet().stream()
				.flatMap(e -> e.getValue().entrySet().stream()
						.flatMap(a -> Stream.of(e.getKey() + "." + a.getKey(), a.getValue())))
				.collect(Collectors.toList()).forEach(s -> System.out.println(s));

		// mainMap.entrySet().stream().flatMap(innerMap->innerMap.getValue())

	}
}
