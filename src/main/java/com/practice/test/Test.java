package com.practice.test;

import java.util.Properties;

class Test {
	

	public static void main(String[] args) {
		try {
			Properties p=System.getProperties();
			
			Float f1=new Float("3.0");
			System.out.println(f1);
			int x=f1.intValue();
			byte b=f1.byteValue();
			double d=f1.doubleValue();
			System.out.println(x+b+d);
		}catch(NumberFormatException e) {
			System.out.println("Bad number");
		}
	}
	
	//Answer=null

}
